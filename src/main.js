import Vue from "vue"
import App from "./App"
import infiniteScroll from "vue-infinite-scroll";

Vue.use(infiniteScroll);
Vue.config.productionTip = false;



/* eslint-disable no-new */
new Vue({
  el: "#app",
  components: { App },
  template: "<App/>"
});
